<?php

use Illuminate\Support\Facades\Route;

Route::middleware('auth')->prefix('articles')->group(function () {
    Route::get('myArticles', 'ArticlesController@myArticles')->name('articles.myArticles');
    Route::get('getMyArticles', 'ArticlesController@getMyArticles')->name('articles.getMyArticles');
    Route::get('getArticleInfo/{id}', 'ArticlesController@getArticleInfo')->name('articles.getArticleInfo');
    Route::post('save', 'ArticlesController@save')->name('articles.save');
    Route::post('update/{id}', 'ArticlesController@update')->name('articles.update');
    Route::delete('remove/{id}', 'ArticlesController@remove')->name('articles.remove');
    Route::get('getLatestArticles', 'ArticlesController@getLatestArticles')->name('articles.getLatestArticles');
    Route::get('getArticleDetail/{id}', 'ArticlesController@getArticleDetail')->name('articles.getArticleDetail');
});