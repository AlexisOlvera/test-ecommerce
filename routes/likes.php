<?php

use Illuminate\Support\Facades\Route;

Route::middleware('auth')->prefix('likes')->group(function () {
    Route::get('like/{article}', 'LikesController@like')->name('likes.like');
    Route::get('notLike/{id}', 'LikesController@notLike')->name('likes.notLike');
    Route::get('seeLikes/{article}', 'LikesController@seeLikes')->name('likes.seeLikes');
});