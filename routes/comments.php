<?php

use Illuminate\Support\Facades\Route;

Route::middleware('auth')->prefix('comments')->group(function () {
    Route::post('save', 'CommentsController@save')->name('comments.save');
});