<div class="modal fade" id="modalMyArticle" tabindex="-1" aria-labelledby="modalMyArticleLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body loading-article">
        
        <form id="articleForm">
          <input type="hidden" id="image64">

          <div class="form-group">
            <input class="form-control" type="text" placeholder="Nombre" name="article" id="article">
          </div>
          <div class="form-group">
            <textarea class="form-control" placeholder="Descripcion" name="description" id="description" rows="3"></textarea>
          </div>
          <div class="form-group">
            <input id="image" name="photo" type="file">
          </div>
        </form>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-0" id="imageExists"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Cerrar</button>
        <button type="button" class="btn btn-sm btn-primary" id="btnSave"><i class="fas fa-check"></i> Guardar</button>
        <button type="button" class="btn btn-sm btn-primary" id="btnUpdate"><i class="fas fa-check"></i> Actualizar</button>
      </div>
    </div>
  </div>  
</div>