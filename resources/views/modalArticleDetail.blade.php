<div class="modal fade" id="modalArticleDetail" tabindex="-1" aria-labelledby="modalArticleDetailLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
      <div class="modal-content">
        <div class="modal-body loading-article-detail">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="mArticleDetail"></div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <hr class="p-0 mt-2 mb-3">
                <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="Escribe un comentario..." aria-describedby="btnWrite" id="comment">
                  <div class="input-group-append">
                    <button class="btn btn-outline-primary" type="button" id="btnWriteComment">Comentar</button>
                  </div>
                </div>
              </div>
              <p class="text-muted pl-3">Comentarios:</p>
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-0 loading-article-comments" id="mArticleComments"></div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Cerrar</button>
        </div>
      </div>
    </div>
  </div>