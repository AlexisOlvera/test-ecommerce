@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <div class="card border-primary loading-latest-articles">
                <div class="card-header text-uppercase">{{ __('Últimas publicaciones') }}</div>
                <div class="card-body container-articles">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="latestArticles"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('modalArticleDetail')
@endsection

@push('css')
    <style>
        .container-articles {
            max-height:45rem;
            overflow-y:auto;
            overflow-x: hidden;
        }
    </style>
@endpush
@push('js')
    <script src="{{ asset('js') }}/home.js"></script>
@endpush
