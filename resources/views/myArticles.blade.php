@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <div class="card border-primary loading-my-articles">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8 text-uppercase">
                            {{ __('Mis articulos publicados') }}
                        </div>
                        <div class="col-4 text-right">
                            <button type="button" class="btn btn-sm btn-primary" id="btnCreateArticle"><i class="fas fa-plus"></i> Agregar articulo</button>
                        </div>
                    </div>
                </div>
                <div class="card-body container-articles">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="myArticles"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('modalMyArticle')
@include('modalLikes')
@endsection

@push('css')
    <style>
        .container-articles {
            max-height:45rem;
            overflow-y:auto;
            overflow-x: hidden;
        }
    </style>
@endpush
@push('js')
<script src="{{ asset('js') }}/myArticles.js"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
{!! JsValidator::formRequest('App\Http\Requests\CreateArticleRequest', '#articleForm'); !!}
@endpush
