<?php

namespace App;

use App\FormatDates;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use FormatDates;
    protected $table = 'articles';
  
    public $timestamps = true;
  
    protected $fillable = [
      'user_id',
      'article',
      'description',
      'photo'
    ];
  
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    public function comments() {
        return $this->hasMany(Comment::class)->orderBy('created_at', 'DESC');
    }
    
    public function likes() {
        return $this->hasMany(Like::class)->orderBy('created_at', 'DESC');
    }
}
