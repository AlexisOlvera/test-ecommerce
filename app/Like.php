<?php

namespace App;

use App\FormatDates;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    use FormatDates;
    protected $table = 'likes';
  
    public $timestamps = true;
  
    protected $fillable = [
      'article_id',
      'user_id',
    ];
  
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function article() {
        return $this->belongsTo(Article::class);
    }
}
