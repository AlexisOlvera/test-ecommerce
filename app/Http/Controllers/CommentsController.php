<?php

namespace App\Http\Controllers;

use App\{Article, Comment};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    public function save(Request $request) {
        try {
            $comment = new Comment;
            $comment->article_id = $request->article;
            $comment->user_id = Auth::user()->id;
            $comment->comment = $request->comment;
            $comment->save();

            $comments = Article::with('comments.user')->find($request->input('article'));

            return response()->json([
                'error' => false,
                'message' => 'Comentario guardado correctamente.',
                'data' => $comments
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ]);
        }
    }
}
