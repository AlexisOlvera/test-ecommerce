<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArticlesController extends Controller
{
    public function myArticles() {
        return view('myArticles');
    }

    public function getMyArticles() {
        $articles = Article::with('user')->where('user_id', Auth::user()->id)->orderBy('created_at', 'DESC')->get();

        return response()->json([
            'error' => false,
            'message' => 'Articulos obtenidos correctamente.',
            'data' => $articles
        ]);
    }

    public function getArticleInfo($id) {
        $article = Article::find($id);

        return response()->json([
            'error' => false,
            'message' => 'Articulo obtenidos correctamente.',
            'data' => $article
        ]);
    }

    public function save(Request $request) {
        try {
            $article = new Article;
            $article->user_id = Auth::user()->id;
            $article->article = $request->article;
            $article->description = $request->description;
            $article->photo = 0;
            $article->save();

            if ($request->b64 && $request->extension) {
                $image = explode(',', $request->b64);
                $imageData = base64_decode($image[1]);
                $imageName = $article->id.'.'.$request->extension;
                $imageRoute = '/images/' . $imageName;

                Storage::disk('public')->put($imageRoute, $imageData);
                $article->photo = $imageName;
                $article->save();
            }

            return response()->json([
                'error' => false,
                'message' => 'Articulo guardado correctamente.',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function update(Request $request, $id) {
        try {
            $article = Article::find($id);
            $article->user_id = Auth::user()->id;
            $article->article = $request->article;
            $article->description = $request->description;
            $article->save();

            if (Storage::disk('public')->exists('/images/'.$article->photo)) {
                if ($request->b64 && $request->extension) {
                    Storage::disk('public')->delete('/images/'.$article->photo);

                    $image = explode(',', $request->b64);
                    $imageData = base64_decode($image[1]);
                    $imageName = $article->id.'.'.$request->extension;
                    $imageRoute = '/images/' . $imageName;

                    Storage::disk('public')->put($imageRoute, $imageData);
                    $article->photo = $imageName;
                    $article->save();
                }
            }

            return response()->json([
                'error' => false,
                'message' => 'Articulo actualizado correctamente.',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function remove($id) {
        try {
            $article = Article::find($id);
            if (Storage::disk('public')->exists('/images/'.$article->photo)) {
                Storage::disk('public')->delete('/images/'.$article->photo);
            }
            $article->destroy($id);

            return response()->json([
                'error' => false,
                'message' => 'Articulo eliminado correctamente.',
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ]);
        }        
    }

    public function getLatestArticles() {
        $articles = Article::with('user')->orderBy('created_at', 'DESC')->get();

        return response()->json([
            'error' => false,
            'message' => 'Articulos obtenidos correctamente.',
            'data' => $articles
        ]);
    }

    public function getArticleDetail($id) {
        $article = Article::with(['user', 'comments.user', 'likes.user'])->find($id);
        $data['article'] = $article;
        $data['localUser'] = Auth::user()->id;

        return response()->json([
            'error' => false,
            'message' => 'Detalle de articulo obtenido correctamente.',
            'data' => $data
        ]);
    }
}
