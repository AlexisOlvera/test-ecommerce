<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikesController extends Controller
{
    public function like($article) {
        $like = new Like;
        $like->article_id = $article;
        $like->user_id = Auth::user()->id;
        $like->save();

        return response()->json([
            'error' => false,
            'message' => 'Like almacenado correctamente.',
            'data' => $like->article_id
        ]);
    }

    public function notLike($id) {
        $like = Like::find($id);
        $articleId = $like->article_id;
        $like->destroy($id);

        return response()->json([
            'error' => false,
            'message' => 'Like eliminado correctamente.',
            'data' => $articleId
        ]);
    }
    
    public function seeLikes($article) {
        $likes = Like::with('user')->where('article_id', $article)->get();

        return response()->json([
            'error' => false,
            'message' => 'Likes obtenidos correctamente.',
            'data' => $likes
        ]);
    }
}
