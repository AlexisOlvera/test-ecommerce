<?php
namespace App;

trait FormatDates {
    protected function serializeDate(\DateTimeInterface $date) {
        return $date->format('d-m-Y H:i');
    }
}