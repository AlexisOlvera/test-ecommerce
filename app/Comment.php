<?php

namespace App;

use App\FormatDates;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use FormatDates;
    protected $table = 'comments';
  
    public $timestamps = true;
  
    protected $fillable = [
      'article_id',
      'user_id',
      'comment',
    ];
  
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function article() {
        return $this->belongsTo(Article::class);
    }
}
