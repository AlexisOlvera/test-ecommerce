<?php

use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('es_ES');
    	foreach (range(1,10) as $i) {
            DB::table('articles')->insert([
                'user_id' => $faker->numberBetween(1,10),
                'article' => $faker->word,
                'description' => $faker->paragraph,
                'photo' => $faker->unique()->numberBetween(1,10).'.jpeg',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
    }
}