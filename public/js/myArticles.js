
var image64 = $('#image64');
$(function() {
    getMyArticles();

    $('#btnCreateArticle').on('click', function () {
        showHideElements('create');
        $('#article').val('');
        $('#description').val('');
        $('#image').val(null);
        image64.removeAttr('data-extension');
        image64.removeAttr('data-result');
        $('.modal-title').html('Agregar articulo');
        $('#modalMyArticle').modal('show');
    });

    $('#btnSave').on('click', function () {
        sendData('save');
    });

    $('#btnUpdate').on('click', function () {
        sendData('update/'+$('#btnUpdate').data('article-id'));
    });

    $("#image").on('change', base64);
});

function base64() {
    image64.attr('data-extension', $('#image').val().split('.').pop());
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.addEventListener('load', function(e) {
            image64.attr('data-result', e.target.result);
        });
        reader.readAsDataURL(this.files[0]);
    }
}

function getMyArticles() {
    $('.loading-my-articles').LoadingOverlay('show', {'size': 5});
    $.ajax({
        url: baseUrl + 'articles/getMyArticles',
        type: 'GET',
    }).done(function (data) {
        render(data.data, '#myArticles');
        $('.loading-my-articles').LoadingOverlay('hide');
    }).fail(function(xhr, status, error) {
        $('.loading-my-articles').LoadingOverlay('hide');
    });
}

function render(data, container) {
    var html = '';
    $(container).html(html);
    if(data.length > 0) {
        html += '<div class="row">';
        data.forEach(function(item) {
            var image = baseUrl + 'storage/images/'+item.photo+'?'+Math.random();
            html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
                        '<div class="card mb-3">'+
                            '<div class="row no-gutters">'+
                                '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 align-self-center">'+
                                    '<img src="'+image+'" class="card-img" alt="...">'+
                                '</div>'+
                                '<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">'+
                                    '<div class="card-body">'+
                                        '<h5 class="card-title text-uppercase">'+item.article+'</h5>'+
                                        '<p class="card-text text-justify">'+item.description+'</p>'+
                                        '<p class="card-text text-right">'+
                                            '<small class="text-muted">Autor: <b>'+item.user.name+'</b></small><br>'+
                                            '<small class="text-muted">Fecha: <b>'+item.created_at+'</b></small>'+
                                        '</p>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right p-1">'+
                                    '<button type="button" class="btn btn-sm btn-dark mr-1" onclick="seeLikes('+item.id+')"><i class="fas fa-eye"></i> Ver likes recibidos</button>'+
                                    '<button type="button" class="btn btn-sm btn-info mr-1" onclick="editArticle('+item.id+')"><i class="fas fa-edit"></i> Editar</button>'+
                                    '<button type="button" class="btn btn-sm btn-danger" onclick="remove('+item.id+')"><i class="fas fa-trash"></i> Eliminar</button>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
        });
        html += '</div>';
    } else {
        html = '<p class="m-3 text-center text-muted">No hay articulos disponibles.</p>';
    }
    $(container).html(html);
}

function editArticle(id) {
    showHideElements('update');
    $('#image').val(null);
    image64.removeAttr('data-extension');
    image64.removeAttr('data-result');
    $('#btnUpdate').removeAttr('data-article-id');
    $('#btnUpdate').attr('data-article-id', id);
    $('.modal-title').html('Editar articulo');
    getArticleInfo(id);
}

function getArticleInfo(id) {
    $('.loading-my-articles').LoadingOverlay('show', {'size': 5});
    $.ajax({
        url: baseUrl + 'articles/getArticleInfo/'+id,
        type: 'GET',
    }).done(function (data) {
        var image = baseUrl + 'storage/images/'+data.data.photo;
        $('#article').val(data.data.article);
        $('#description').val(data.data.description);
        $('#imageExists').html('<div class="card">'+
                                    '<div class="card-body p-0">'+
                                        '<p class="card-text"><small class="text-muted ml-2">Imagen actual, seleccione otra para actualizarla:</small></p>'+
                                    '</div>'+
                                    '<img src="'+image+'" class="card-img-bottom" alt="...">'+
                                '</div>');
        $('#modalMyArticle').modal('show');
        $('.loading-my-articles').LoadingOverlay('hide');
    }).fail(function(xhr, status, error) {
        $('.loading-my-articles').LoadingOverlay('hide');
    });
}

function remove(id) {
    Swal.fire({
        title: '¿Está seguro de eliminar este articulo?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        reverseButtons: true,
        buttonsStyling: false,
        customClass: {
            confirmButton: 'btn btn-info',
            cancelButton: 'btn btn-danger mx-2',
        }
    }).then((result) => {
        if (result.isConfirmed) {
            $('.loading-my-articles').LoadingOverlay('show', {'size': 5});
            $.ajax({
                headers: { 'X-CSRF-TOKEN': csrfToken },
                url: baseUrl + 'articles/remove/'+id,
                type: 'DELETE',
            }).done(function (data) {
                if (data.error) {
                    notify('fas fa-exclamation-circle', 'Error:', data.message, 'danger', 1500);
                } else {
                    notify('fas fa-check-circle', '', data.message, 'success', 1500);
                    getMyArticles();
                }
                $('.loading-my-articles').LoadingOverlay('hide');
            }).fail(function(xhr, status, error) {
                $('.loading-my-articles').LoadingOverlay('hide');
            });
        }
    });
}

function sendData(url) {
    if ($('#articleForm').valid()) {
        var article = $('input[name="article"]').val();
        var description = $('#description').val();
        var extension = image64.data('extension') ? image64.data('extension') : null;
        var b64 = image64.data('result') ? image64.data('result') : null;

        $('.loading-article').LoadingOverlay("show", {'size': 5});
        $.ajax({
            headers: { 'X-CSRF-TOKEN': csrfToken },
            url: baseUrl + 'articles/' + url,
            type: 'POST',
            dataType: 'json',
            data: {
                article: article,
                description: description,
                extension: extension,
                b64: b64,
            },
        }).done(function (data) {
            if (data.error) {
                notify('fas fa-exclamation-circle', 'Error:', data.message, 'danger', 1500);
            } else {
                notify('fas fa-check-circle', '', data.message, 'success', 1500);
                getMyArticles();
                $('#modalMyArticle').modal('hide');
            }
            $('.loading-article').LoadingOverlay("hide");
        }).fail(function(xhr, status, error) {
            $('.loading-article').LoadingOverlay("hide");
        });
    }
}

function showHideElements(type) {
    switch (type) {
        case 'create':
            $('#btnSave').show();
            $('#btnUpdate').hide();
            $('#imageExists').hide();
        break;
        case 'update':
            $('#btnSave').hide();
            $('#btnUpdate').show();
            $('#imageExists').show();
        break;
    }
}

function seeLikes(article) {
    $('.loading-my-articles').LoadingOverlay('show', {'size': 5});
    $.ajax({
        url: baseUrl + 'likes/seeLikes/'+article,
        type: 'GET',
    }).done(function (data) {
        var html = '';
        if(data.data.length > 0) {
            html += '<ul class="list-group">';
            data.data.forEach(function(item) {
                html += '<li class="list-group-item">Nombre: <b>'+item.user.name+'</b> <br> Fecha: <b>'+item.created_at+'</b></li>';
            });
            html += '</ul>';
        } else {
            html = '<p class="m-3 text-center text-muted">Este articulo aún no ha recibido likes.</p>';
        }
        $('#likes').html(html);
        $('#modalLikes').modal('show');
        $('.loading-my-articles').LoadingOverlay('hide');
    }).fail(function(xhr, status, error) {
        $('.loading-my-articles').LoadingOverlay('hide');
    });
}