$(function() {
    getLatestArticles();

    $('#btnWriteComment').on('click', function () {
        var comment = $('#comment').val();
        if (comment.trim()) {
            saveComment(comment);
        } else {
            notify('fas fa-exclamation-circle', 'Error:', 'Debe ingresar un comentario.', 'danger', 1500);
        }
    });
});

function getLatestArticles() {
    $('.loading-latest-articles').LoadingOverlay('show', {'size': 5});
    $.ajax({
        url: baseUrl + 'articles/getLatestArticles',
        type: 'GET',
    }).done(function (data) {
        render(data.data, '#latestArticles', 'latest');
        $('.loading-latest-articles').LoadingOverlay('hide');
    }).fail(function(xhr, status, error) {
        $('.loading-latest-articles').LoadingOverlay('hide');
    });
}

function render(data, container, type) {
    var html = '';
    $(container).html(html);
    if(data.length > 0) {
        html += type == 'latest' ? '<div class="row">' : '';
        data.forEach(function(item) {
            switch (type) {
                case 'latest':
                    var image = baseUrl + '/storage/images/'+item.photo;
                    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" onclick="articleDetail('+item.id+')" style="cursor:pointer!important;">'+
                                '<div class="card mb-3">'+
                                    '<div class="row no-gutters">'+
                                        '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 align-self-center">'+
                                            '<img src="'+image+'" class="card-img" alt="...">'+
                                        '</div>'+
                                        '<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">'+
                                            '<div class="card-body">'+
                                                '<h5 class="card-title text-uppercase">'+item.article+'</h5>'+
                                                '<p class="card-text text-justify">'+item.description+'</p>'+
                                                '<p class="card-text text-right">'+
                                                    '<small class="text-muted">Autor: <b>'+item.user.name+'</b></small><br>'+
                                                    '<small class="text-muted">Fecha: <b>'+item.created_at+'</b></small>'+
                                                '</p>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';
                break;
                case 'comments':
                    html += '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
                                '<div class="card border-light mb-1">'+
                                    '<div class="card-body p-1">'+
                                        '<div class="row">'+
                                            '<div class="col-12 text-left">'+
                                                '<small class="text-muted"><b>'+item.user.name+'</b></small>'+
                                            '</div>'+
                                        '</div>'+
                                        '<p class="card-text text-justify">'+item.comment+'</p>'+
                                    '</div>'+
                                    '<div class="card-footer border-light p-1 text-right">'+
                                        '<small class="text-muted"><b>'+item.created_at+'</b></small>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';
                break;
            }
        });
        html += type == 'latest' ? '</div>' : '';
    } else {
        switch (type) {
            case 'latest':
                html = '<p class="m-3 text-center text-muted">No hay articulos disponibles.</p>';
            break;
            case 'comments':
                html = '<p class="m-3 text-center text-muted">No hay comentarios disponibles.</p>';
            break;
        }
    }
    $(container).html(html);
}

function articleDetail(id) {
    getArticleDetail(id);
    $('#modalArticleDetail').modal('show');
}

function getArticleDetail(id) {
    $('.loading-article-detail').LoadingOverlay('show', {'size': 5});
    $.ajax({
        url: baseUrl + 'articles/getArticleDetail/'+id,
        type: 'GET',
    }).done(function (data) {
        renderArticleDetail(data.data);
        $('.loading-article-detail').LoadingOverlay('hide');
    }).fail(function(xhr, status, error) {
        $('.loading-article-detail').LoadingOverlay('hide');
    });
}

function renderArticleDetail(data) {
    var dArticle = data.article;
    var dLocalUser = data.localUser;
    $('#mArticleDetail').html('');
    $('#mArticleDetail').removeAttr('data-article-id');
    $('#mArticleDetail').attr('data-article-id', dArticle.id);
    var totalLikes = dArticle.likes.length;
    var youLike = false, likeOrNot, action, likeId = 0, newTotal, persons, textLikes;
    if(dArticle.likes.length > 0) {
        dArticle.likes.forEach(function(item) {
            if (item.user_id == dLocalUser) {
                youLike = true;
                likeId = item.id;
            }
        });
    }

    if (youLike) {
        newTotal = totalLikes - 1;
        persons = newTotal == 1 ? 'persona le' : 'personas les';
        textLikes = '<small class="text-primary">A tí y a '+newTotal+' '+persons+' gusta esto.</small>';
        action = '<i onclick="notLike('+likeId+')" class="far fa-thumbs-up mr-1 text-primary text-bold" style="cursor:pointer!important;"></i>';
    } else {
        persons = totalLikes == 1 ? 'persona le' : 'personas les';
        textLikes = '<small class="text-primary">A '+totalLikes+' '+persons+' gusta esto.</small>';
        action = '<i onclick="like('+dArticle.id+')" class="far fa-thumbs-up mr-1" style="cursor:pointer!important;"></i>';
    }

    var image = baseUrl + '/storage/images/'+dArticle.photo;
    $('#mArticleDetail').html('<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 loading-article-like">'+
                            '<div class="card mb-3">'+
                                '<div class="row no-gutters">'+
                                    '<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 align-self-center">'+
                                        '<img src="'+image+'" class="card-img" alt="...">'+
                                    '</div>'+
                                    '<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">'+
                                        '<div class="card-body">'+
                                            '<h5 class="card-title text-uppercase">'+dArticle.article+'</h5>'+
                                            '<p class="card-text text-justify">'+dArticle.description+'</p>'+
                                            '<p class="card-text text-right">'+
                                                '<small class="text-muted">Autor: <b>'+dArticle.user.name+'</b></small><br>'+
                                                '<small class="text-muted">Fecha: <b>'+dArticle.created_at+'</b></small><br>'+
                                                action + textLikes +
                                            '</p>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>');
    render(dArticle.comments, '#mArticleComments', 'comments');
}

function like(article) {
    $.ajax({
        url: baseUrl + 'likes/like/'+article,
        type: 'GET',
    }).done(function (data) {
        getArticleDetail(data.data);
    }).fail(function(xhr, status, error) {
    });
}

function notLike(id) {
    $.ajax({
        url: baseUrl + 'likes/notLike/'+id,
        type: 'GET',
    }).done(function (data) {
        getArticleDetail(data.data);
    }).fail(function(xhr, status, error) {
    });
}

function saveComment(comment) {
    $('.loading-article-comments').LoadingOverlay('show', {'size': 5});
    $.ajax({
        headers: { 'X-CSRF-TOKEN': csrfToken },
        url: baseUrl + 'comments/save',
        type: 'POST',
        dataType: 'json',
        data: { 
            comment: comment,
            article: $('#mArticleDetail').data('article-id')
        },
    }).done(function (data) {
        if (data.error) {
            notify('fas fa-exclamation-circle', 'Error:', data.message, 'danger', 1500);
        } else {
            $('#comment').val('');
            notify('fas fa-check', '', data.message, 'success', 1500);
            render(data.data.comments, '#mArticleComments', 'comments');
        }
        $('.loading-article-comments').LoadingOverlay('hide');
    }).fail(function(xhr, status, error) {
        $('.loading-article-comments').LoadingOverlay('hide');
    });
}