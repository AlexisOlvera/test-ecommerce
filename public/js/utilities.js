const baseUrl = $('#app').data('url');
const csrfToken = $('meta[name="csrf-token"]').attr('content');

function notify(icon, title, message, type, time) {
    $.notify({
        icon: icon,
        title: title,
        message: message,
    }, {
        element: 'body',
        type: type ? type : 'default',
        allow_dismiss: true,
        placement: {
            from: 'top',
            align: 'right'
        },
        offset: {
            x: 15,
            y: 15
        },
        spacing: 10,
        z_index: 1080,
        delay: time ? time :2500,
        timer: time ? time : 5000,
        url_target: '_blank',
        mouse_over: 'pause',
        animate: {
            enter: 'animated bounceIn',
            exit: 'animated bounceOut'
        },
        template: '<div data-notify="container" class="alert alert-dismissible alert-{0} alert-notify" role="alert">' +
            '<span class="alert-icon" data-notify="icon"></span> ' +
            '<div class="alert-text"</div> ' +
                '<span class="alert-title" data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
            '</div>' +
            '<button type="button" class="close" data-notify="dismiss" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '</div>'
    });
}